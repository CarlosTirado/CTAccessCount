﻿using System;
using CTAccessCount.Logic.Base;
using CTAccessCount.Logic.Interactors;
using Interactors.CTAccessCount.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CTAccessCount.Test
{
    [TestClass]
    public class CTAccessCountTest
    {
        [TestMethod]
        public void TestRegisterEntry()
        {
            RegisterEntryRequest registerEntryRequest = new RegisterEntryRequest();
            registerEntryRequest.Date = DateTime.Now;
            registerEntryRequest.Username = "1065000001";

            RegisterEntryInteractor registerEntryInteractor = new RegisterEntryInteractor();
            InteractorResponse registerEntryResponse = registerEntryInteractor.Execute(registerEntryRequest);

            Assert.AreEqual(false, registerEntryResponse.Error);
        }

        [TestMethod]
        public void TestGetEntriesInfoBetweenDates()
        {
            GetEntriesInfoBetweenDatesRequest registerEntryRequest = new GetEntriesInfoBetweenDatesRequest();
            registerEntryRequest.InitialDate = new DateTime(2018,1,1);
            registerEntryRequest.FinalDate = DateTime.Now;

            GetEntriesInfoBetweenDatesInteractor getEntriesInfoBetweenDatesInteractor = new GetEntriesInfoBetweenDatesInteractor();
            GetEntriesInfoBetweenDatesResponse entriesInfoBetweenDates = (GetEntriesInfoBetweenDatesResponse) getEntriesInfoBetweenDatesInteractor.Execute(registerEntryRequest);

            Assert.AreEqual(false, entriesInfoBetweenDates.Error);
            Assert.AreNotEqual(0, entriesInfoBetweenDates.AccessPerDays.Count);
        }

        [TestMethod]
        public void TestGetEntriesInfoOnADay()
        {
            GetEntriesInfoOnADayRequest registerEntryRequest = new GetEntriesInfoOnADayRequest();
            registerEntryRequest.Date = DateTime.Now;

            GetEntriesInfoOnADayInteractor getEntriesInfoOnADayInteractor = new GetEntriesInfoOnADayInteractor();
            GetEntriesInfoOnADayResponse entriesInfoOnADay = (GetEntriesInfoOnADayResponse)getEntriesInfoOnADayInteractor.Execute(registerEntryRequest);

            Assert.AreEqual(false, entriesInfoOnADay.Error);
            Assert.AreNotEqual(0, entriesInfoOnADay.AccessDetailsPerDays.Count);
        }

        [TestMethod]
        public void TestGetEntriesInfoFromDate()
        {
            GetEntriesInfoBetweenDatesRequest registerEntryRequest = new GetEntriesInfoBetweenDatesRequest();
            registerEntryRequest.InitialDate = new DateTime(2018, 1, 1);
            registerEntryRequest.FinalDate = null;

            GetEntriesInfoBetweenDatesInteractor getEntriesInfoBetweenDatesInteractor = new GetEntriesInfoBetweenDatesInteractor();
            GetEntriesInfoBetweenDatesResponse entriesInfoBetweenDates = (GetEntriesInfoBetweenDatesResponse)getEntriesInfoBetweenDatesInteractor.Execute(registerEntryRequest);

            Assert.AreEqual(false, entriesInfoBetweenDates.Error);
            Assert.AreNotEqual(0, entriesInfoBetweenDates.AccessPerDays.Count);
        }
    }
}
