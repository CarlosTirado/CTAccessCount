﻿using CTAccessCount.Data.Models;
using CTAccessCount.Logic.Base;
using CTAccessCount.Logic.Views;
using CTUtil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTAccessCount.Logic.Interactors
{
    public class GetEntriesInfoBetweenDatesInteractor : TemplateInteractor
    {
        GetEntriesInfoBetweenDatesRequest _request => (GetEntriesInfoBetweenDatesRequest)request;
        GetEntriesInfoBetweenDatesResponse _response => (GetEntriesInfoBetweenDatesResponse)response;
        protected override bool Validations()
        {
            response = new GetEntriesInfoBetweenDatesResponse();

            _request.InitialDate = _request.InitialDate.Date;


            if (_request.InitialDate.Date < new DateTime(1900, 1, 1))
            {
                _response.Error = true;
                _response.Message = "The field 'Initial Date' must be completed";
                return false;
            }

            if (_request.FinalDate != null)
            {
                _request.FinalDate = _request.FinalDate.Value.Date;

                if (_request.FinalDate.Value.Date < new DateTime(1900, 1, 1))
                {
                    _response.Error = true;
                    _response.Message = "The field 'Final Date' must be completed";
                    return false;
                }
            }


            return true;
        }
        protected override void Process()
        {
            IQueryable<AccessPerDay> queryAccessPerDays = context.AccessPerDays.Where(t => t.Date >= _request.InitialDate);
            if (_request.FinalDate != null) queryAccessPerDays.Where(t => t.Date <= _request.FinalDate);

            List<AccessPerDay> accessPerDays = queryAccessPerDays.ToList();

            List<AccessPerDayView> accessPerDayViews = CTMapper.Map<AccessPerDay, AccessPerDayView>(accessPerDays);
            _response.AccessPerDays = accessPerDayViews;
        }
        protected override void SaveChange() { }
    }
    public class GetEntriesInfoBetweenDatesRequest : InteractorRequest
    {
        public DateTime InitialDate { get; set; }
        public DateTime? FinalDate { get; set; }
    }
    public class GetEntriesInfoBetweenDatesResponse : InteractorResponse
    {
        public GetEntriesInfoBetweenDatesResponse() : base()
        {
            AccessPerDays = new List<AccessPerDayView>();
        }
        public List<AccessPerDayView> AccessPerDays { get; set; }
    }
}
