﻿using CTAccessCount.Data;
using CTAccessCount.Data.Models;
using CTAccessCount.Logic.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interactors.CTAccessCount.Logic
{
    public class RegisterEntryInteractor : TemplateInteractor
    {
        RegisterEntryRequest _request => (RegisterEntryRequest) request;
        RegisterEntryResponse _response => (RegisterEntryResponse) response;
        public RegisterEntryInteractor()
        {
            context = new AccessAcountContext();
            response = new RegisterEntryResponse();
        }
        protected override bool Validations()
        {
            return true;
        }
        protected override void Process()
        {
            RealizarRegistroDeAcceso();
        }
        private void RealizarRegistroDeAcceso()
        {
            AccessPerDay accessPerDay = context.AccessPerDays.FirstOrDefault(t => t.Date.Year == _request.Date.Year && t.Date.Month == _request.Date.Month && t.Date.Day == _request.Date.Day);
            if (accessPerDay != null)
            {
                accessPerDay.Count++;
            }
            else
            {
                accessPerDay = new AccessPerDay();
                accessPerDay.Date = DateTime.Now.Date;
                accessPerDay.Count = 1;
                context.AccessPerDays.Add(accessPerDay);
            }
            accessPerDay.AccessDetailsPerDay.Add(CreateAccessDetailsPerDay());
        }
        private AccessDetailsPerDay CreateAccessDetailsPerDay()
        {
            AccessDetailsPerDay accessDetailsPerDay = new AccessDetailsPerDay();
            accessDetailsPerDay.Date = _request.Date;
            accessDetailsPerDay.Username = _request.Username;
            return accessDetailsPerDay;
        }
    }
    public class RegisterEntryRequest : InteractorRequest
    {
        public string Username { get; set; }
        public DateTime Date { get; set; }
    }
    public class RegisterEntryResponse : InteractorResponse
    {
        public RegisterEntryResponse()
        {
            Error = false;
            Message = "The register has been successfully";
        }

        public bool Error { get; set; }
        public string Message { get; set; }
    }
}
