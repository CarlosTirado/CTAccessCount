﻿using CTAccessCount.Data.Models;
using CTAccessCount.Logic.Base;
using CTAccessCount.Logic.Views;
using CTUtil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTAccessCount.Logic.Interactors
{
    public class GetEntriesInfoOnADayInteractor : TemplateInteractor
    {
        GetEntriesInfoOnADayRequest _request => (GetEntriesInfoOnADayRequest)request;
        GetEntriesInfoOnADayResponse _response => (GetEntriesInfoOnADayResponse)response;
        protected override bool Validations()
        {
            response = new GetEntriesInfoOnADayResponse();

            _request.Date = _request.Date.Date;

            if (_request.Date.Date < new DateTime(1900, 1, 1))
            {
                _response.Error = true;
                _response.Message = "The field 'Date' must be completed";
                return false;
            }

            return true;
        }
        protected override void Process()
        {
            AccessPerDay accessPerDays = context.AccessPerDays.FirstOrDefault(t => t.Date.Year == _request.Date.Year && t.Date.Month == _request.Date.Month && t.Date.Day == _request.Date.Day);
            if (accessPerDays != null)
            {
                List<AccessDetailsPerDayView> accessDetailsPerDayViews = CTMapper.Map<AccessDetailsPerDay, AccessDetailsPerDayView>(accessPerDays.AccessDetailsPerDay.ToList());
                _response.AccessDetailsPerDays = accessDetailsPerDayViews;
            }
            
        }
        protected override void SaveChange() {}
    }
    public class GetEntriesInfoOnADayRequest : InteractorRequest
    {
        public DateTime Date { get; set; }
    }
    public class GetEntriesInfoOnADayResponse : InteractorResponse
    {
        public GetEntriesInfoOnADayResponse() : base()
        {
            AccessDetailsPerDays = new List<AccessDetailsPerDayView>();
        }
        public List<AccessDetailsPerDayView> AccessDetailsPerDays { get; set; }
    }
}
