﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTAccessCount.Logic.Views
{
    public class AccessDetailsPerDayView
    {
        public long Id { get; set; }
        public long AccessPerDayId { get; set; }
        public DateTime Date { get; set; }
        public string Username { get; set; }
    }
}
