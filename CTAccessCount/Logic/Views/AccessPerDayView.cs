﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTAccessCount.Logic.Views
{
    public class AccessPerDayView
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public long Count { get; set; }
    }
}
