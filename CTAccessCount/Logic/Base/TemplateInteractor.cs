﻿using CTAccessCount.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTAccessCount.Logic.Base
{
    public abstract class TemplateInteractor
    {
        protected AccessAcountContext context;
        protected InteractorRequest request;
        protected InteractorResponse response;

        public TemplateInteractor()
        {
            context = new AccessAcountContext();
        }
        public InteractorResponse Execute(InteractorRequest _request)
        {
            request = _request;
            try
            {
                if (Validations())
                {
                    Process();
                    SaveChange();
                }
            }
            catch(Exception Exception)
            {
                response.Error = true;
                response.Message = Exception.Message;
            }
            return response;
        }
        protected abstract bool Validations();
        protected abstract void Process();
        protected virtual void SaveChange()
        {
            context.SaveChanges();
        }
    }
    public class InteractorResponse
    {
        public InteractorResponse()
        {
            Error = false;
            Message = "The register has been successfully";
        }

        public string Message { get; set; }
        public bool Error { get; set; }
    }
    public class InteractorRequest
    {
    }
}
