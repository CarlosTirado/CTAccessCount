﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTAccessCount.Data.Models
{
    public class AccessDetailsPerDay
    {
        [Key]
        public long Id { get; set; }
        public long AccessPerDayId { get; set; }
        public DateTime Date { get; set; }
        public string Username { get; set; }

        [ForeignKey("AccessPerDayId")]
        public virtual AccessPerDay AccessPerDay { get; set; }
    }
}