﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTAccessCount.Data.Models
{
    public class AccessPerDay
    {
        public AccessPerDay()
        {
            AccessDetailsPerDay = new HashSet<AccessDetailsPerDay>();
        }

        [Key]
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public long Count { get; set; }

        public virtual ICollection<AccessDetailsPerDay> AccessDetailsPerDay { get; set; }
    }
}
