﻿using CTAccessCount.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTAccessCount.Data
{
    public class AccessAcountContext : DbContext
    {
        public AccessAcountContext() : base("AccessAcountContext") { }

        public DbSet<AccessPerDay> AccessPerDays { get; set; }
        public DbSet<AccessDetailsPerDay> AccessDetailsPerDays { get; set; }
    }
}
