﻿using CTAccessCount.Logic.Base;
using CTAccessCount.Logic.Interactors;
using CTAccessCount.Logic.Views;
using Interactors.CTAccessCount.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTAccessCount
{
    public class HandlerAccessCount
    {
        public InteractorResponse RegisterEntry(DateTime Date, string Username)
        {
            RegisterEntryRequest registerEntryRequest = new RegisterEntryRequest();
            registerEntryRequest.Date = Date;
            registerEntryRequest.Username = Username;

            RegisterEntryInteractor registerEntryInteractor = new RegisterEntryInteractor();
            return registerEntryInteractor.Execute(registerEntryRequest);
        }

        public GetEntriesInfoBetweenDatesResponse GetEntriesInfoBetweenDates(DateTime InitialDate, DateTime FinalDate)
        {
            GetEntriesInfoBetweenDatesRequest registerEntryRequest = new GetEntriesInfoBetweenDatesRequest();
            registerEntryRequest.InitialDate = InitialDate;
            registerEntryRequest.FinalDate = FinalDate;

            GetEntriesInfoBetweenDatesInteractor getEntriesInfoBetweenDatesInteractor = new GetEntriesInfoBetweenDatesInteractor();
            GetEntriesInfoBetweenDatesResponse entriesInfoBetweenDates = (GetEntriesInfoBetweenDatesResponse) getEntriesInfoBetweenDatesInteractor.Execute(registerEntryRequest);
            return entriesInfoBetweenDates;
        }

        public GetEntriesInfoOnADayResponse GetEntriesInfoOnADay(DateTime Date)
        {
            GetEntriesInfoOnADayRequest registerEntryRequest = new GetEntriesInfoOnADayRequest();
            registerEntryRequest.Date = Date;

            GetEntriesInfoOnADayInteractor getEntriesInfoOnADayInteractor = new GetEntriesInfoOnADayInteractor();
            GetEntriesInfoOnADayResponse entriesInfoOnADay = (GetEntriesInfoOnADayResponse)getEntriesInfoOnADayInteractor.Execute(registerEntryRequest);
            return entriesInfoOnADay;
        }

        public GetEntriesInfoBetweenDatesResponse GetEntriesInfoFromDate(DateTime InitialDate)
        {
            GetEntriesInfoBetweenDatesRequest registerEntryRequest = new GetEntriesInfoBetweenDatesRequest();
            registerEntryRequest.InitialDate = InitialDate;
            registerEntryRequest.FinalDate = null;

            GetEntriesInfoBetweenDatesInteractor getEntriesInfoBetweenDatesInteractor = new GetEntriesInfoBetweenDatesInteractor();
            GetEntriesInfoBetweenDatesResponse entriesInfoBetweenDates = (GetEntriesInfoBetweenDatesResponse)getEntriesInfoBetweenDatesInteractor.Execute(registerEntryRequest);
            return entriesInfoBetweenDates;
        }
    }
}
